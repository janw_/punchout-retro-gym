# Punch-Out!! Retro Gym Environment

![Punch-Out!! scene](./images/screenshot.png "Punch-Out!! scene")

## Description

An environment implementation of Nintendo's famous game [Punch-Out!! (1987)](https://en.wikipedia.org/wiki/Punch-Out!!_(NES)) for [OpenAI's Retro Gym](https://openai.com/blog/gym-retro/). The Retro Gym framework can be used to train agents to play the game through [reinforcement learning](https://en.wikipedia.org/wiki/Reinforcement_learning).

## Installation

Copy `PunchOut-Nes` directory into retro:
`cp -r ./PunchOut-Nes somewhere/site-packages/retro/data/stable/`.

## Notes

- The reward function as described by `/PunchOut-Nes/script.lua` might need tweaking for optimal convergence.

- Adding other RAM locations to `/PunchOut-Nes/data.json` might be needed for optimal convergence.

## License

See `LICENSE`.